const registryURL  = 'https://registry.npmjs.org/-/v1';
let themes = {
    objects: null
};

let plugins = {
    objects: null
};


const registryError = (msg) => {
    console.error(`An error occurred while trying to call the registry: ${msg}.`);
}

const registryWarning = (msg) => {
    console.warn(`An error occurred while trying to call the registry: ${msg}. The parameter has been removed from the API call.`);
}

/**
 * Retrieve all plugins available from npmjs registry
 */
const getPlugins = async () => {
    const nb = 250;
    const p = await searchFromRegistry('peertube-plugin-', nb);
    plugins.objects = p.objects;
 
   if (p.total > p.objects.length) {
        const n = Math.round(p.total / nb);
        
        let i = 1;
        for (i; i < n; i++) {
            plugins.objects = plugins.objects.concat((await searchFromRegistry('peertube-plugin-', nb, nb * i)).objects);
        }
    }

    console.log(`${plugins.objects.length} plugins retrieved from npmjs registry!`);
} 

/**
 * Retrieve all themes available from npmjs registry
 */
const getThemes = async () => {
    const nb = 250;
    const t = await searchFromRegistry('peertube-theme-', nb);
    themes.objects = t.objects;
 
   if (t.total > t.objects.length) {
        const n = Math.round(t.total / nb);
        
        let i = 1;
        for (i; i < n; i++) {
            themes.objects = themes.objects.concat((await searchFromRegistry('peertube-theme-', nb, nb * i)).objects);
        }
    }

    console.log(`${themes.objects.length} themes retrieved from npmjs registry!`);
} 

const searchFromRegistry = async (text = null, size = null, from = null, quality = null, popularity = null, maintenance = null) => {
    let searchEndpoint = `${registryURL}/search?`;

    if (text !== null) {
        if (typeof text === 'string') {
            searchEndpoint += `text=${encodeURIComponent(text)}&`;
        } else {
            registryWarning('text parameter must be a string');
        }
    }
    
    if (size !== null) {
        if (typeof size === 'number' && Number.isSafeInteger(size)) {
               searchEndpoint += `size=${size}&`;
        } else {
            registryWarning('size parameter must be an integer');
        }
    }
    
    if (from !== null) {
        if (typeof from === 'number' && Number.isSafeInteger(from)) {
               searchEndpoint += `from=${from}&`;
        } else {
            registryWarning('from parameter must be an integer');
        }
    }
    
    if (quality !== null) {
        if (typeof quality === 'number') {
            searchEndpoint += `quality=${quality}&`;
        } else {
            registryWarning('quality parameter must be a float');
        }
    }
    
    if (popularity !== null) {
        if (typeof popularity === 'number') {
            searchEndpoint += `popularity=${popularity}&`;
        } else {
            registryWarning('popularity parameter must be a float');
        }
    }
    
    if (maintenance !== null) {
        if (typeof maintenance === 'number') {
            searchEndpoint += `maintenance=${maintenance}&`;
        } else {
            registryWarning('maintenance parameter must be a float');
        }
    }

    searchEndpoint = searchEndpoint.substring(0, searchEndpoint.length - 1);

    let res = (await fetch(searchEndpoint)).json();
    let obj = (await res).objects.filter((o) => {
        return o.package.name.startsWith("peertube-plugin") || o.package.name.startsWith("peertube-theme");
    });

    return {objects: obj, total: (await res).total};
};

const handleSearch = (search, type, thoughTags = true) => {
    if (type !== "theme" && type !== "plugin") {
        registryError('You must provide a valid search type');
        return null;
    }

    if (typeof search === 'string') {
        let res = [];
        const tp = type == 'theme' ? themes : plugins;

        search = search.split(' ');

        let j = 0;
        for (j; j < tp.objects.length; j++) {
            let termsFound = true; // All terms must be found in title
            let tagsFound = false; // Only one term must be found in tags

            let i = 0;
            for (i; i < search.length; i++) {
                // Search corresponding titles
                if (!tp.objects[j].package.name.toLowerCase().includes(search[i].toLowerCase())) {
                    termsFound = false;
                }

                // Search corresponding tags
                if (thoughTags) {
                    if (tp.objects[j].package.keywords.find(e => {
                        return e.toLowerCase().includes(search[i].toLowerCase())
                    }) !== undefined) {
                        tagsFound = true;
                    }
                }
            }
            

            if (termsFound || tagsFound) {
                res.push(tp.objects[j]);
            }
        }

        return res;
    } else {
        registryError('search parameter must be a string');
    }
} 

/**
 * Search through the plugins list
 * 
 * @param {string | null} search 
 * @returns 
 */
const searchPlugins = async (search = null) => {
    if (search !== null) {
        return handleSearch(search, 'plugin');
    }
    
    return null;
};

const searchThemes = async (search = null) => {
    if (search !== null) {
        return handleSearch(search, 'theme');
    }
    
    return null;
};
 